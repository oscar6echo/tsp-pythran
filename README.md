# TSP Simulated Annealing

## Accelerated with Pythran + OMP and/or concurrent.futures

> This repo contains the code and guides in support of medium article [Pythran: Python at C++ Speed](TBD)

## 1 - Overview

### 1.1 - Problem and Algo

The well known [Traveling Salesman problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem) (TSP) consists in finding the shortest path that goes through each city once exactly and returns to the starting point.

Here we use the [Simulated Annealing algo](https://en.wikipedia.org/wiki/Simulated_annealing) to look for a good - not necessarily the best - solution in predictable time.

The mutations applied at each step are - for i, j random path positions:

- Reverse path between cities in i-th and j-th position
- Move city in i-th position to j-th position
- Swap city in i-th position with city in j-th position

For a more precise description see [tsp_compute_single_threaded.py](https://gitlab.com/oscar6echo/tsp-pythran/blob/master/tsp_compute_single_threaded.py).

### 1.2 - Notebooks and Results

For the user interface and some results see the demo notebooks [demo-notebook-tsp-pythran.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/tsp-pythran/raw/master/demo-notebook-tsp-pythran.ipynb).

## 2 - Pythran

In order to accelerate the algo, written in Python, I used [Pythran](https://pythran.readthedocs.io/en/latest/) which is really very convenient - as outlined by this article: [Pythran as a bridge between fast prototyping and code deployment](http://serge-sans-paille.github.io/pythran-stories/pythran-as-a-bridge-between-fast-prototyping-and-code-deployment.html).

To parallel the TSP search I used 2 additional techniques:

- [concurrent.futures](https://docs.python.org/3/library/concurrent.futures.html) on top of Pythran a accelerated module - code in branch `master`
- [OMP](https://bisqwit.iki.fi/story/howto/openmp/) through Pythran - code in branch `omp`

I ran the code on:

- macOS - laptop and desktop
- Linux - GCP VM Ubuntu 18:04

## 3 - Install

### 3.1 macOS

Here are the steps I took to install it on macOS:

- Install compiler with [homebrew](https://brew.sh/):

```bash
$ brew install llvm
```

- Create a segregated conda environment to contain Pythran:

```bash
$ conda create -n pythran python=3 -y
$ source activate pythran
$ conda install -c conda-forge pythran -y

# install other packages
$ conda install pandas matplotlib notebook -y
```

- Create a `~/.pythranrc` file:

```bash
[compiler]
cflags=-std=c++11 -fno-math-errno -w
ldflags=-L/usr/local/opt/llvm/lib # added according to `llvm info`
CC=/usr/local/opt/llvm/bin/clang # brew installed clang path
CXX=/usr/local/opt/llvm/bin/clang++ # brew installed clang++ path
```

### 3.2 Linux VM

Here are step-by-step guides to setup a cloud VM to run a remote Jupyter are:

- on [GCP](cloud-vm/gcp.md)
- on [Azure](cloud-vm/azure.md)

## 4 - Compile

The annotation to the Python function that needs acceleration is _extremely_ simple:

```python
## tsp_compute.py

# pythran export search_for_best(int, float list list, int, float, int, float, float)

def search_for_best(seed,
                    cities,
                    nb_step,
                    beta_mult=1.005,
                    accept_nb_step=100,
                    p1=0.2,
                    p2=0.6):
    """
    exported
    """
    # etc
```

And so are the compile instructions. Note that they are different if you use OMP directives:

```bash
$ source activate pythran

# regular compilation
(pythran) $ pythran -Ofast -march=native tsp_compute_single_threaded.py

# omp conscious compilation
(pythran) $ pythran -DUSE_XSIMD -fopenmp -march=native tsp_compute_multi_threaded_omp.py
```

## 5 - concurrent.futures

OMP is low level and enables fine control of parallelism. But may also be a bit tricky.

For coarser parallelism across all a machine CPUs, as in the case of this algo, concurrent futures is a very efficient alternative.

See [tsp_concurrent.py](https://gitlab.com/oscar6echo/tsp-pythran/blob/master/tsp_concurrent.py) for an example of how to use `ProcessPoolExecutor`.

In order to know how many cores you Mac this command is useful:

```bash
# macOS
$ sysctl hw.physicalcpu hw.logicalcpu
# linux
$ lscpu | grep -E '^Thread|^Core|^Socket|^CPU\('
```

## 4 - Conclusion

The speedup are:

- from Pythran for one Simulated Annealing search: **x~10**
- from concurrent.futures for multiples of 8 (#cores) searches: **x~8**

That is a whopping **x~80** !  
For a very reasonable effort.

Speed recorded:

- Desktop (iMac 2016): **50s**
- Laptop (McBook Pro 2017): **110s**
- Google Cloud n1-highcpu-32: **17s** (cost = 0.8 \$/hour)
