
from compute_bar import bar

n = int(1e4)
seed = 123456
k = 4

myset_as_list, count = bar(n, seed, k)

print(count, len(myset_as_list))
print(sorted(myset_as_list))
