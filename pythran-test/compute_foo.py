
############
# to compile
# $ pythran -Ofast -march=native compute_foo.py


#pythran export foo(int, int)

import random as rd

def foo(n, s):
    rd.seed(s)
    myset = set()
    count = 0
    for i in range(n):
        # generate random tuple mytuple - represents a permutation e.g. (1, 3, 2, 4, 0)
        # will generate the same mytuple often
        mytuple = (rd.randint(1, 10), rd.randint(1, 10))
        if mytuple not in myset:
            myset.add(mytuple)
            count += 1
    return myset, count

