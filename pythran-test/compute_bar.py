
############
# to compile
# $ pythran -Ofast -march=native compute_bar.py


# pythran export bar(int, int, int)

import random as rd


def bar(n, s, k):
    rd.seed(s)
    myset = set()
    count = 0
    permutation = [i for i in range(k)]
    for i in range(n):
        rd.shuffle(permutation)
        sig = signature(tuple(permutation))
        if sig not in myset:
            myset.add(sig)
            print(sig)
            count += 1
    # return myset, count
    return list(myset), count


def signature(perm):
    i = perm.index(0)
    sig_1 = perm[i:]+perm[:i]
    sig_2 = sig_1[0:1]+sig_1[1:][::-1]

    if sig_1[1] < sig_2[1]:
        sig = sig_1
    else:
        sig = sig_2

    return tuple(sig)
