
from compute_foo import foo

n = int(1e3)
seed = 123456

myset_as_list, count = foo(n, seed)

print(count, len(myset_as_list))
print(sorted(myset_as_list))
