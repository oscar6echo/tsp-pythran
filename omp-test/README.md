## Check OMP install

This section contains the simplest OMP test.  
Compile and run to check if OMP is properly installed.

- Compile:

```bash
$ /usr/local/opt/llvm/bin/clang -fopenmp -L/usr/local/opt/llvm/lib omp_hello.c -o hello
```

- Run:

```bash
$ ./hello
Hello from thread 0, nthreads 4
Hello from thread 3, nthreads 4
Hello from thread 2, nthreads 4
Hello from thread 1, nthreads 4
```
