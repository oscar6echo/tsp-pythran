#!/bin/bash

# this script assume homebrew and miniconda3 are installed


export ENV_NAME=pythran
export ANACONDA=miniconda3


echo "---------------------------------------------------"
echo "Installing llvm from brew"

brew install llvm


echo "---------------------------------------------------"
echo "(re)creating env $ENV_NAME..."

conda env remove -n $ENV_NAME -y
conda create -n $ENV_NAME python=3 -y


echo "---------------------------------------------------"
echo "switching to env $ENV_NAME"
source activate $ENV_NAME
echo "Python path: $(which python)"

conda install -c conda-forge pythran -y
conda install notebook matplotlib pandas -y
# conda install pylint autopep8 -y # while working


echo "---------------------------------------------------"
echo "creating kernelspec for env $ENV_NAME"

# user install
python -m ipykernel install --user --name $ENV_NAME --display-name $ENV_NAME
source deactivate


echo "---------------------------------------------------"
echo "creating ~/.pythranrc"

echo "
[compiler]
ldflags=-L/usr/local/opt/llvm/lib
cflags=-std=c++11 -fno-math-errno -w
CC=/usr/local/opt/llvm/bin/clang2
CXX=/usr/local/opt/llvm/bin/clang++
" > ~/.pythranrc


echo "---------------------------------------------------"
echo "env $ENV_NAME created"
echo "To activate this env, use:"
echo "> source activate $ENV_NAME"






