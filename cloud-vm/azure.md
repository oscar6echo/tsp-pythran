# How to set up a remote Jupyter on Microsoft Azure

### 0 - Prerequisite

You need:

- Internet access
- a Microsoft identity - You can get a [free account](https://azure.microsoft.com/en-us/free/)

### 1 - Install azure CLI command line tool on local machine

- See [Azure CLI install guide](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
- First sign in ([doc](https://docs.microsoft.com/en-us/cli/azure/get-started-with-azure-cli?view=azure-cli-latest#sign-in)):

```bash
az login
```

- Then create a resource group ([doc](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/quick-create-cli#create-a-resource-group)):

```bash
az group create --name TestingPythran --location westeurope
```

- Check your groups:

```bash
az group list
```

### 2 - Create remote VM on Azure Cloud

My recommendation is to it from the the [Azure portal](https://portal.azure.com) the first time:

- Go to [Azure portal](https://portal.azure.com)
- Go to create a resource / create a Virtual Machine
- Create a VM. There is a [lot of choice](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/sizes).

For [Pythran](TBD) tests I picked a **Standard_H8** type (it seems difficult to get more that 10 vCPUs)

If you use the portal you must supply an SSH public key. For info about ssh independently of Azure see the [GitHub help page](https://help.github.com/articles/connecting-to-github-with-ssh/).

After you have generated ssh keys (see directory `~/.ssh`), you can display the public key with:

```
cat ~/.ssh/id_rsa.pub
```

Alternatively you can create the same VM with Azure CLI:

```bash
az vm create --resource-group TestingPythran --name remote-jupyter --admin-username oscar6echo --image UbuntuLTS --size Standard_H8 --generate-ssh-keys --verbose
```

This command will also create an ssh key pair, if necessary, or use existing ones in `/.ssh/id_rsa.pub`.
In addition it will also create a `default-allow-ssh` rule in networking rules.

### 4 - Check remote VM is up and running

- From the [Azure console](https://console.cloud.google.com/) in Home > Virtual Machines
- Or command line:

```bash
az vm show --name remote-jupyter --resource-group TestingPythran
```

### 5 - Log in remote VM from local machine

Using the ssh keys created in previous step.

- Terminal:

```bash
# format admin-usernmae@ipaddress
ssh oscar6echo@157.56.177.39
```

You can get the exact command from the Azure portal under the VM screen after you click **Connect**.

_WARNINGS_:

- Make sure the VM networking rules allow at least inbound and outbound ssh traffic from/to your local machine.
- Note that if you use the Azure cloud shell, you must still upload your ssh keys to login your VM from it.

### 6 - Install software on remote VM

The followings instructions must be run on your remote RM.

```bash
# update package manager
sudo apt-get update

# install standard utilities
sudo apt-get -y install bzip2 wget git

# install compiler
sudo apt-get -y install g++

# download miniconda linux (see https://repo.continuum.io/miniconda/)
# pin version if prefered
miniconda="Miniconda3-latest-Linux-x86_64.sh"
wget -P Downloads/ https://repo.continuum.io/miniconda/${miniconda}
# install miniconda - accept default options except yes to prepend miniconda path to PATH
bash ~/Downloads/${miniconda}

# run .bashrc to update path
. ~/.bashrc

# check miniconda python is first in path
which python

# create env and install packages
conda create -n pythran python=3 -y
source activate pythran
conda install -c conda-forge pythran -y
conda install notebook matplotlib pandas -y

# Note: do 'conda install' before 'pip install'
# Why ? See https://www.anaconda.com/blog/developer-blog/using-pip-in-a-conda-environment/

# git clone repo tsp-pythran
git clone https://gitlab.com/oscar6echo/tsp-pythran.git
cd tsp-pythran
```

The VM is all set.

### 7 - Set up port forwarding on local machine

- Forward a local port (8888) to the server’s port (8888) where the notebook server is running:

```bash
# format: username@ipaddress -NL 9888:localhost:9888
ssh oscar6echo@157.56.177.39 -NL 9888:localhost:9888
```

- For more info on ssh tunnels see [this article](https://solitum.net/an-illustrated-guide-to-ssh-tunnels/)

### 8 - Launch jupyter on remote VM

- Launch Jupyter

```bash
jupyter notebook --no-browser --port=9888
```

- The terminal will show something like:

```bash
(pythran) Olivier@remote-jupyter:~/tsp-pythran$ jupyter notebook --no-browser --port=9888
[I 16:29:44.714 NotebookApp] Writing notebook server cookie secret to /run/user/1001/jupyter/notebook_cookie_secret
[I 16:29:44.924 NotebookApp] Serving notebooks from local directory: /home/Olivier/tsp-pythran
[I 16:29:44.925 NotebookApp] The Jupyter Notebook is running at:
[I 16:29:44.925 NotebookApp] http://localhost:9888/?token=2690aa8b2f21dac4e0b71f6470d5c69587f2f77c424843c9
[I 16:29:44.925 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 16:29:44.928 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///run/user/1001/jupyter/nbserver-8713-open.html
    Or copy and paste one of these URLs:
        http://localhost:9888/?token=2690aa8b2f21dac4e0b71f6470d5c69587f2f77c424843c9
```

- Copy the last url including the token

### 9 - Open browser local machine

- Open web browser and paste in the address bar

You should see the usual Jupyter file explorer screen.  
Ready to go !

### 10 - Stop or delete remote VM when you are finished

> Be careful it is all too easy to forget !

The Standard_H8 VM clocks at —0.9 €/hour which is small if you use parsimoniously and can grow large for an individual if you leave it idle.

To completely stop costs, **delete the VM**. It is fast to create again if you keep all commands in bash files.  
However if you plan to use intermittently I found the best tradeoff was to only probably to **stop the VM**. You instantly stop paying for the CPU and only keep paying for storage which is very little. This way you can restart it with simple click in a few seconds, and resume work where you left.
