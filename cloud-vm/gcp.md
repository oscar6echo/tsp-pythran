# How to set up a remote Jupyter on Google Cloud

### 0 - Prerequisite

You need:

- Internet access
- a Google identity - you have one with a gmail account for example

### 1 - Install gcloud command line tool on local machine

- See [gcould install guide](https://cloud.google.com/sdk/docs/quickstarts)
- First authenticate ([doc](https://cloud.google.com/sdk/gcloud/reference/auth/login)):

```bash
gcloud auth login
```

- Then set up your environment ([doc](https://cloud.google.com/sdk/gcloud/reference/init)):

```bash
gcloud init
```

- Finally check your config ([doc](https://cloud.google.com/sdk/gcloud/reference/config/list)):

```bash
gcloud config list
```

- To update your config:

```bash
# examples

# to set core/project
gcloud config set project testing-pythran

# to set core/zone
gcloud config set compute/zone us-east1-b
```

which results in:

```bash
$ gcloud config list
[compute]
zone = us-east1-b
[core]
account = firstname.lastname@gmail.com
disable_usage_reporting = False
project = testing-pythran
```

### 2 - Create new project on Google Cloud

My recommendation is to it from the the [gcloud console](https://console.cloud.google.com/) the first time:

- Go to [gcloud console](https://console.cloud.google.com/)
- Create a new project e.g. testing-pythran
- Enable billing for this project, if necessary
  - See [Google help page](https://support.google.com/cloud/answer/6288653#new-billing)
  - Check out [Google Platform Free Tier](https://cloud.google.com/free/)

Next time you can simply the gcloud command line ([doc](https://cloud.google.com/sdk/gcloud/reference/projects/create)), for example:

```bash
gcloud projects create "myuniqueeprojectname" --name "my project human readable name"
```

### 3 - Create remote VM on Google Cloud

My recommendation is to it from the the [gcloud console](https://console.cloud.google.com/) the first time:

- Go to Compute Engine / VM instances
- Create a VM. There is a [lot of choice](https://cloud.google.com/compute/docs/machine-types). You are asked about the following main characteristics
  - name
  - zone (choose your area, obviously - this cannot be changed later)
  - machine type, CPU and memory
  - Boot disk
  - firewall rules

One you have customized your machine to your taste, you can get the equivalent REST of command line instructions, at the bottom of the creation page.

For [Pythran](TBD) tests I picked an **n1-highcpu-32** type.

### 4 - Check remote VM is up and running

- From the [gcloud console](https://console.cloud.google.com/) in Compute Engine / VM instances
- Or command line:

```bash
gcloud compute instances list
```

### 5 - Create ssh keys on local machine

- In terminal run the following command. It will check if an RSA Pub-Prv key pair exists or create one if not.

```bash
gcloud compute config-ssh
```

- Check result in `~/.ssh`

```bash
cat config # human readable info
cat google_compute_engine # RSA private key
cat google_compute_engine.pub # RSA public key
cat google_compute_known_hosts # Google remote machines confirmed as known by user
```

- For more info about gcloud ssh instructions: see the [doc](https://cloud.google.com/sdk/gcloud/reference/compute/config-ssh)
- For more info about ssh independently of gcloud see the [github help page](https://help.github.com/articles/connecting-to-github-with-ssh/) for example.

### 6 - Log in remote VM from local machine

Using the ssh keys created in previous step.

- Terminal:

```bash
# format: ssh VM-instance-name.zone.project
ssh remote-jupyter.us-east1-b.testing-pythran
```

- Alternative syntax using gcloud:

```bash
# to copy/paste from the google console
gcloud compute --project "testing-pythran" ssh --zone "us-east1-b" "remote-jupyter"
```

_Note_: The ssh keys are not necessary if you log in the VM from the [in-browser cloud shell](https://cloud.google.com/shell/docs/quickstart) since you are already identified.

### 7 - Install software on remote VM

The followings instructions must be run on your remote RM.

```bash
# update package manager
sudo apt-get update

# install standard utilities
sudo apt-get -y install bzip2 wget git

# install compiler
sudo apt-get -y install g++

# download miniconda linux (see https://repo.continuum.io/miniconda/)
# pin version if prefered
miniconda="Miniconda3-latest-Linux-x86_64.sh"
wget -P Downloads/ https://repo.continuum.io/miniconda/${miniconda}
# install miniconda - accept default options except yes to prepend miniconda path to PATH
bash ~/Downloads/${miniconda}

# run .bashrc to update path
. ~/.bashrc

# check miniconda python is first in path
which python

# create env and install packages
conda create -n pythran python=3 -y
source activate pythran
conda install -c conda-forge pythran -y
conda install notebook matplotlib pandas -y

# Note: do 'conda install' before 'pip install'
# Why ? See https://www.anaconda.com/blog/developer-blog/using-pip-in-a-conda-environment/

# git clone repo tsp-pythran
git clone https://gitlab.com/oscar6echo/tsp-pythran.git
cd tsp-pythran
```

The VM is all set.

### 8 - Set up port forwarding on local machine

- Forward a local port (8888) to the server’s port (8888) where the notebook server is running:

```bash
ssh remote-jupyter.us-east1-b.testing-pythran -NL 9888:localhost:9888
```

- Alternative syntax:

```bash
gcloud compute --project "testing-pythran" ssh --zone "us-east1-b" "remote-jupyter" -NL 9888:localhost:9888
```

- For more info on ssh tunnels see [this article](https://solitum.net/an-illustrated-guide-to-ssh-tunnels/)

### 9 - Launch jupyter on remote VM

- Launch Jupyter

```bash
jupyter notebook --no-browser --port=9888
```

- The terminal will show something like:

```bash
(pythran) Olivier@remote-jupyter:~/tsp-pythran$ jupyter notebook --no-browser --port=9888
[I 16:29:44.714 NotebookApp] Writing notebook server cookie secret to /run/user/1001/jupyter/notebook_cookie_secret
[I 16:29:44.924 NotebookApp] Serving notebooks from local directory: /home/Olivier/tsp-pythran
[I 16:29:44.925 NotebookApp] The Jupyter Notebook is running at:
[I 16:29:44.925 NotebookApp] http://localhost:9888/?token=2690aa8b2f21dac4e0b71f6470d5c69587f2f77c424843c9
[I 16:29:44.925 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 16:29:44.928 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///run/user/1001/jupyter/nbserver-8713-open.html
    Or copy and paste one of these URLs:
        http://localhost:9888/?token=2690aa8b2f21dac4e0b71f6470d5c69587f2f77c424843c9
```

- Copy the last url including the token

### 10 - Open browser local machine

- Open web browser and paste in the address bar

You should see the usual Jupyter file explorer screen.  
Ready to go !

### 11 - Stop or delete remote VM when you are finished

> Be careful it is all too easy to forget !

The **n1-highcpu-32** VM clocks at —0.8 \$/hour which is small if you use parsimoniously and can grow large for an individual if you leave it idle.

To completely stop costs, **delete the VM**. It is fast to create again if you keep all commands in bash files.  
However if you plan to use intermittently I found the best tradeoff was to only probably to **stop the VM**. You instantly stop paying for the CPU and only keep paying for storage which is very little. This way you can restart it with simple click in a few seconds, and resume work where you left.

For the exact billing of stopped VMs see the [billing_for_stopped_instances page](https://cloud.google.com/compute/docs/instances/stopping-or-deleting-an-instance#billing_for_stopped_instances)

If you delete a VM, you might want to take a snapshot of the VM persistent disk to quickly back up the disk so you can recover lost data, transfer contents to a new disk. See the [create-snapshots page](https://cloud.google.com/compute/docs/disks/create-snapshots). Essentially snapshots are very cheap and fast.
