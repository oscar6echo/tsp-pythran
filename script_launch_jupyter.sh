#!/bin/bash

export ENV_NAME=pythran


echo "---------------------------------------------------"
echo "switching to env $ENV_NAME"
source activate $ENV_NAME
echo "Python path: $(which python)"



echo "---------------------------------------------------"
echo "launching jupyter"

jupyter notebook --no-browser --port=9888 --no-browser

