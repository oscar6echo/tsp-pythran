#!/bin/bash

# this script assume gcc and miniconda3 are installed


export ENV_NAME=pythran
export ANACONDA=miniconda3

echo "---------------------------------------------------"
echo "(Re)creating env $ENV_NAME..."


echo "---------------------------------------------------"
echo "(re)creating env $ENV_NAME..."

conda env remove -n $ENV_NAME -y
conda create -n $ENV_NAME python=3 -y


echo "---------------------------------------------------"
echo "switching to env $ENV_NAME"
source activate $ENV_NAME
echo "Python path: $(which python)"

pip install numpy
pip install pythran

pip install notebook matplotlib pandas
# pip install pylint autopep8 # while working


echo "---------------------------------------------------"
echo "creating kernelspec for env $ENV_NAME"

# user install
python -m ipykernel install --user --name $ENV_NAME --display-name $ENV_NAME
source deactivate

echo "---------------------------------------------------"
echo "env $ENV_NAME created"
echo "To activate this env, use:"
echo "> source activate $ENV_NAME"






